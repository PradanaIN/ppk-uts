# PPK-UTS : Aplikasi Sederhana yang Mengonsumsi Rest API Webservice

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Soal

Anda diminta untuk membuat aplikasi sederhana dengan tema pilihan Anda dengan spesifikasi
sebagai berikut.
1. Pengguna aplikasi dibagi dua level yaitu admin dan member, yang masing-masing memiliki hak akses berbeda.
2. Admin dapat melakukan operasi create, read, update, dan delete untuk semua data.
3. Member dapat melakukan register dan login. Member yang terautentikasi hanya diberikan hak akses untuk read dan update data.
4. Field untuk register minimal: Nama Lengkap, Username, Email, dan Password. Anda bisa menambahkan field lainnya yang sesuai dengan tema aplikasi Anda.
5. Khusus untuk Username dan Email harus ada pengecekan dahulu apakah sudah terdaftar sebelumnya. Jika sudah terdaftar, maka registrasi tidak bisa dilakukan. Email juga harus dicek dengan format email yang valid.
6. Tambahkan fitur untuk mengubah profil pengguna. Semua profil pengguna dapat diubah kecuali username.
7. Tambahkan fitur lainnya sesuaikan dengan tema pilihan Anda.

## Ketentuan:
1. Aplikasi dibangun berbasis front-end dan back-end.
2. Aplikasi back-end/server menggunakan web service berbasis RestFul API.
3. Aplikasi front-end/client berupa web page (GUI) yang mengkonsumsi data RestFul API. Front-end dapat dibangun dengan Javascript PHP, atau lainnya.
4. Terapkan token-based authentication dan authorization untuk proses login dan pemberian hak akses.

