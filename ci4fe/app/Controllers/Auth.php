<?php

namespace App\Controllers;

class Auth extends BaseController
{
    public function register()
    {
        $client = \Config\Services::curlrequest();

        $url = "http://localhost/uts-ppk/ci4be/public/register";

        $form_params = [
            'nama' => $this->request->getVar('nama'),
            'username' => $this->request->getVar('username'),
            'email' => $this->request->getVar('email'),
            'password' => $this->request->getVar('password')
        ];


        $response = $client->request('post', $url, ['http_errors' => false, 'form_params' => $form_params]);

        return redirect()->to('/login');
    }

    // login
    public function login()
    {
        $client = \Config\Services::curlrequest();
        $url_login = "http://localhost/uts-ppk/ci4be/public/login";
        $form_params = [
            'email' => $this->request->getVar('email'),
            'password' => $this->request->getVar('password')
        ];
        $response_login = $client->request('post', $url_login, ['http_errors' => false, 'form_params' => $form_params]);
        $response_login = json_decode($response_login->getBody(), true);
        $token = $response_login['token'];

        //store a token session
        session()->set('token', $token);

        //id_user
        $headers = [
            'Authorization' => 'Bearer ' . $token
        ];
        $url_id_user = "http://localhost/uts-ppk/ci4be/public/me";
        $response_id_user = $client->request('get', $url_id_user, ['headers' => $headers, 'http_errors' => false]);
        $response_id_user = json_decode($response_id_user->getBody(), true);
        $id_user = $response_id_user['id'];

        //store a id session
        session()->set('id_user', $id_user);

        //setrole
        $url_data_by_id = "http://localhost/uts-ppk/ci4be/public/user/" . $id_user;
        $response_data_by_id = $client->request('get', $url_data_by_id, ['headers' => $headers, 'http_errors' => false, 'form_params' => $form_params]);
        $response_data_by_id = json_decode($response_data_by_id->getBody(), true);
        $email_by_id = $response_data_by_id['email'];
        $nama_user = $response_data_by_id['nama'];
        $username_user = $response_data_by_id['username'];
        if ($email_by_id == "222011436@stis.ac.id") {
            session()->set('admin', "true");
        }

        //store a nama session
        session()->set('nama_user', $nama_user);
        session()->set('username_user', $username_user);
        return redirect()->to('/beranda');
    }

    // logout
    public function logout()
    {
        session()->remove('token');
        session()->destroy();
        return redirect()->to('/login');
    }
}
