<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class EventClient extends BaseController
{
    // get all events from api
    // public function index()
    // {
    //     if (session()->get('token')) {
    //         helper('restclient');
    //         $url = "http://localhost/uts-ppk/ci4be/public/event";

    //         $data = [
    //             'title' => 'Daftar Kegiatan',
    //             'event' => akses_restapi('GET', $url, []),
    //             'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
    //         ];

    //         dd($data);
    //         return view('pages/daftar-kegiatan', $data);
    //     } else {
    //         return redirect()->to('/login');
    //     }
    // }

    // add new event
    public function addEventForm()
    {
        if (session()->get('token')) {
            helper(['restclient']);
            $data = [
                'title' => 'Tambah Data Kegiatan',
                'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            return view('pages/add_event', $data);
        } else {
            return redirect()->to('/login');
        }
    }

    public function addEvent()
    {
        $client = \Config\Services::curlrequest();

        if (session()->get('token')) {

            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/event';

            $form = [
                'nama' => $this->request->getVar('nama'),
                'tanggal' => $this->request->getVar('tanggal'),
                'waktu' => $this->request->getVar('waktu'),
                // 'sampul' => $namaSampul,
                'narahubung' => $this->request->getVar('narahubung'),
                'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            $response = $client->request('POST', $url, ['http_errors' => false, 'form_params' => $form]);

            return redirect()->to('/daftar-kegiatan');
        } else {
            return redirect()->to('/login');
        }
    }

    // edit event
    public function updateEventForm($id)
    {

        if (session()->get('token')) {
            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/event/' . $id;

            $data = [
                'title' => 'Ubah Kegiatan',
                'event' => akses_restapi('GET', $url, []),
                'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            return view('pages/edit_event', $data);
        } else {
            return redirect()->to('/login');
        }
    }

    public function updateEvent($id)
    {
        $client = \Config\Services::curlrequest();

        if (session()->get('token')) {
            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/event/' . $id;

            $form = [
                'id' => $id,
                'nama' => $this->request->getVar('nama'),
                'tanggal' => $this->request->getVar('tanggal'),
                'waktu' => $this->request->getVar('waktu'),
                'narahubung' => $this->request->getVar('narahubung'),
                // 'sampul' => $namaSampul,
                'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            $response = $client->request('PUT', $url, ['http_errors' => false, 'form_params' => $form]);

            return redirect()->to('/daftar-kegiatan');
        } else {
            return redirect()->to('/login');
        }
    }

    // delete event
    public function deleteEvent($id)
    {
        $client = \Config\Services::curlrequest();

        if (session()->get('token')) {
            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/event/' . $id;

            akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', []);

            $response = $client->request('DELETE', $url, ['http_errors' => false]);

            return redirect()->to('/daftar-kegiatan');
        } else {
            return redirect()->to('/login');
        }
    }

    // Event Deails 
    // public function show($id)
    // {
    //     if (session()->get('token')) {
    //         helper(['restclient']);
    //         $url = 'http://localhost/uts-ppk/ppk-uts-backend/public/buku/' . $id;

    //         $data = [
    //             'title' => 'Detail Buku',
    //             'active' => 'daftarbuku',
    //             'buku' => akses_restapi('GET', $url, []),
    //             'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ppk-uts-backend/public/me', [])
    //         ];

    //         if (empty($data['buku'])) {
    //             throw new \CodeIgniter\Exceptions\PageNotFoundException('Buku dengan id ' . $id . ' tidak ditemukan');
    //         }

    //         return view('/buku/detail', $data);
    //     } else {
    //         return redirect()->to('/login');
    //     }
    // }
}
