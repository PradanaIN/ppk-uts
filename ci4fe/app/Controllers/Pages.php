<?php

namespace App\Controllers;

class Pages extends BaseController
{
    // index controller
    public function index()
    {
        $data = [
            'title' => 'Beranda'
        ];

        echo view('pages/index', $data);
    }

    public function beranda()
    {
        $data = [
            'title' => 'Beranda'
        ];

        echo view('pages/index', $data);
    }

    // home controller
    public function dashboard()
    {
        $data = [
            'title' => 'Dashboard'
        ];

        echo view('pages/dashboard', $data);
    }

    //list kegiatan controller
    public function list_event()
    {
        $data = [
            'title' => 'Daftar Kegiatan'
        ];

        echo view('pages/list_event', $data);
    }

    // add controller
    // public function add_event()
    // {
    //     $data = [
    //         'title' => 'Tambah Kegiatan'
    //     ];
    //     echo view('pages/add_event', $data);
    // }

    // update controller
    // public function edit_event()
    // {
    //     $data = [
    //         'title' => 'Edit Kegiatan'
    //     ];

    //     echo view('pages/edit_event', $data);
    // }

    // delete controller
    // public function delete_event()
    // {
    //     $data = [
    //         'title' => 'Hapus Kegiatan'
    //     ];

    //     echo view('pages/delete_event', $data);
    // }

    // profile controller
    public function profile()
    {
        $data = [
            'title' => 'Profil'
        ];

        echo view('pages/profile', $data);
    }

    // edit profile controller
    public function edit_profile()
    {
        $data = [
            'title' => 'Edit Profil'
        ];

        echo view('pages/edit_profil', $data);
    }

    // login controller
    public function login()
    {
        echo view('pages/login');
    }

    // logout controller
    public function logout()
    {
        echo view('pages/logout');
    }

    // register controller
    public function register()
    {
        echo view('pages/register');
    }
}
