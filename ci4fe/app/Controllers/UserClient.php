<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class UserClient extends BaseController
{
    // edit event
    public function updateProfilForm($id)
    {

        if (session()->get('token')) {
            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/user/' . $id;

            $data = [
                'title' => 'Ubah Kegiatan',
                'user' => akses_restapi('GET', $url, []),
                'akun' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            return view('pages/edit_profil', $data);
        } else {
            return redirect()->to('/login');
        }
    }

    public function updateProfil($id)
    {
        $client = \Config\Services::curlrequest();

        if (session()->get('token')) {
            helper(['restclient']);
            $url = 'http://localhost/uts-ppk/ci4be/public/user/' . $id;

            $form = [
                'id' => $id,
                'nama' => $this->request->getVar('nama'),
                'username' => $this->request->getVar('username'),
                'email' => $this->request->getVar('email'),
                'password' => $this->request->getVar('password'),
                // 'sampul' => $namaSampul,
                'user' => akses_restapi('GET', 'http://localhost/uts-ppk/ci4be/public/me', [])
            ];

            $response = $client->request('PUT', $url, ['http_errors' => false, 'form_params' => $form]);

            return redirect()->to('/profil');
        } else {
            return redirect()->to('/login');
        }
    }
}
