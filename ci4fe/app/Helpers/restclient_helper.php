<?php

function akses_restapi($method, $url, $data)
{
    $client = \Config\Services::curlrequest();

    $token   = session()->get('token');

    $headers = [
        'Authorization' => 'Bearer ' . $token
    ];

    $response = $client->request($method, $url, [
        'headers' => $headers,
        'http_errors' => false,
        'form_params' => $data
    ]);

    return json_decode($response->getBody(), true);
}
