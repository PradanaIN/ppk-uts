<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>


<div class="container">
    <h2>Tambah Kegiatan</h2>
    <form method="post" enctype="multipart/form-data" action="/tambah">

        <?= csrf_field(); ?>

        <div class="form-group">
            <label>Nama Kegiatan</label>
            <input type="text" class="form-control" name="nama" id="nama" required />
        </div>
        <div class="form-group">
            <label>Tanggal Pelaksanaan</label>
            <input type="date" class="form-control" name="tanggal" id="tanggal" required />
        </div>
        <div class="form-group">
            <label>Waktu Pelaksanaan</label>
            <input type="text" class="form-control" name="waktu" id="waktu" required />
        </div>
        <div class="form-group">
            <label>Narahubung</label>
            <input type="text" class="form-control" name="narahubung" id="narahubung" required />
        </div>

        <button class="btn btn-primary" type="submit" name="tambah">Tambah Kegiatan</button>

    </form>
</div>

<?= $this->endSection(); ?>