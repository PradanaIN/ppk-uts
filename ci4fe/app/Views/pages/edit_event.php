<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <h2>Edit Kegiatan</h2>
    <form method="post" enctype="multipart/form-data" action="/update-kegiatan/<?= $event['id']; ?>">

        <?= csrf_field(); ?>

        <div class="form-group">
            <label>Nama Kegiatan</label>
            <input type="text" class="form-control" name="nama" id="nama" value="<?= $event['nama']; ?>" />
        </div>
        <div class="form-group">
            <label>Tanggal Pelaksanaan</label>
            <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?= $event['tanggal']; ?>" />
        </div>
        <div class="form-group">
            <label>Waktu Pelaksanaan</label>
            <input type="text" class="form-control" name="waktu" id="waktu" value="<?= $event['waktu']; ?>" />
        </div>
        <div class="form-group">
            <label>Narahubung</label>
            <input type="text" class="form-control" name="narahubung" id="narahubung" value="<?= $event['narahubung']; ?>" />
        </div>

        <button class="btn btn-primary" type="submit" name="edit">Edit Kegiatan</button>

    </form>
</div>
<?= $this->endSection(); ?>