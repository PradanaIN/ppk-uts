<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
                <div class="col">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Update Profile</h1>
                        </div>
                        <form class="user" method="post" enctype="multipart/form-data" action="/update-profil/<?= $user['id']; ?>">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="exampleInputFisrtName" placeholder="Nama Lengkap" name="nama" value="<?= $user['nama']; ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="exampleInputLastName" placeholder="Username" name="username" onfocus="blur()" value="<?= $user['username']; ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Alamat Email" name="email" value="<?= $user['email']; ?>">
                            </div>
                            <div class=" form-group">
                                <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password" name="password" required>
                            </div>
                            <br>
                            <button class="btn btn-primary" type="submit" name="edit">Edit Profil</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->endSection(); ?>