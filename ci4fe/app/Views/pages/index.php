<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row gy-4">
        <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center">
            <h1>Sistem Informasi Kegiatan Mahasiswa (Sigma)</h1>
            <h2>Politeknik Statistika STIS</h2>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img">
            <img src="https://stis.ac.id/media/source/optk20.jpeg" class="img-fluid" alt="">
        </div>
    </div>
</div>


<div class="container mt-5 mb-5">

    <div class="row justify-content-between">
        <div class="col-lg-5 d-flex align-items-center justify-content-center about-img">
            <img src="https://stis.ac.id/media/source/IMG_0222.JPG" class="img-fluid" alt="" data-aos="zoom-in">
        </div>
        <div class="col-lg-6 pt-5 pt-lg-0">
            <h3 data-aos="fade-up">Tentang Sigma</h3>
            <p class="text-justify" data-aos="fade-up" data-aos-delay="100">
                Sistem Informasi Kegiatan Mahasiswa (Sigma) diharapkan mampu memudahkan mahasiswa Politeknik Statistika STIS
                untuk mendapatkan berbagai informasi mengenai kegiatan-kegiatan baik kegiatan internal yang diselenggarakan oleh
                Badan Pusat Statistik, Politeknis Statistika STIS, Organisasi Mahasiswa, Unit Kegiatan atau Komunitas Mahasiswa,
                dan Kepanitiaan Mahasiswa maupun kegiatan eksternal yang diselenggarakan oleh K/L/I Lain dengan tujuan untuk
                memaksimalkan angka partisipasi mahasiswa pada kegiatan yang diadakan.
            </p>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>