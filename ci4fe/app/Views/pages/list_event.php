<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<?php if (session()->getFlashdata('pesan')) : ?>
    <div class="alert alert-success alert-dismissible show fade">
        <?= session()->getFlashdata('pesan'); ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

<?php endif; ?>

<!-- DataTales Example -->
<?php if (session()->get('admin')) { ?>
    <div class=" card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h3 class=" m-0 font-weight-bold text-primary">Daftar Kegiatan</h3>
            <a class="btn btn-success" href=<?= base_url("/tambah-kegiatan") ?>>
                <i class="fas fa-plus"> &nbsp; Tambah Kegiatan</i>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Kegiatan</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Narahubung</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Kegiatan</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Narahubung</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $client = \Config\Services::curlrequest();
                        $token = session()->get('token');
                        if (!$token) {
                            return redirect()->to('/login');
                        }
                        $headers = [
                            'Authorization' => 'Bearer ' . $token
                        ];
                        $url = "http://localhost/uts-ppk/ci4be/public/event";
                        $response = $client->request('GET', $url, ['headers' => $headers, 'http_errors' => false]);
                        $dataArray = json_decode($response->getBody(), true);
                        $i = 1;
                        foreach ($dataArray['event'] as $row) :  ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $row['nama']; ?></td>
                                <td><?= $row['tanggal']; ?></td>
                                <td><?= $row['waktu']; ?></td>
                                <td><?= $row['narahubung']; ?></td>
                                <td>
                                    <a class="btn btn-primary" href=<?= base_url("/edit-kegiatan") . "/" . $row['id']; ?>>
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger" href=<?= base_url("/hapus-kegiatan") . "/" . $row['id']; ?>>
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php } ?>

<?php if (!session()->get('admin')) { ?>
    <div class=" card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h3 class=" m-0 font-weight-bold text-primary">Daftar Kegiatan</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Kegiatan</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Narahubung</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Kegiatan</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Narahubung</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $client = \Config\Services::curlrequest();
                        $token = session()->get('token');
                        if (!$token) {
                            return redirect()->to('/login');
                        }
                        $headers = [
                            'Authorization' => 'Bearer ' . $token
                        ];
                        $url = "http://localhost/uts-ppk/ci4be/public/event";
                        $response = $client->request('GET', $url, ['headers' => $headers, 'http_errors' => false]);
                        $dataArray = json_decode($response->getBody(), true);
                        $i = 1;
                        foreach ($dataArray['event'] as $row) :  ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $row['nama']; ?></td>
                                <td><?= $row['tanggal']; ?></td>
                                <td><?= $row['waktu']; ?></td>
                                <td><?= $row['narahubung']; ?></td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php } ?>

<?= $this->endSection(); ?>