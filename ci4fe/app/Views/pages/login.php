<?php if (session()->getFlashdata('pesan')) : ?>
    <div class="alert alert-success col-md-3" role="alert">
        <?= session()->getFlashdata('pesan'); ?>
    </div>
<?php endif; ?>

<?php
$emailError = null;
$passwordError = null;
?>

<?php if (session()->getFlashdata('errors')) :
    $pesanErrors = session()->getFlashdata('errors');

    if (isset($pesanErrors['email'])) {
        $emailError = $pesanErrors['email'];
    }

    if (isset($pesanErrors['password'])) {
        $passwordError = $pesanErrors['password'];
    }


endif; ?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login Sigma</title>

    <!-- Custom fonts for this template-->
    <link href=<?= base_url("vendor/fontawesome-free/css/all.min.css") ?> rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href=<?= base_url("css/sb-admin-2.min.css") ?> rel="stylesheet">
    <link href="https://stis.ac.id/media/source/up.png" rel="icon">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Login Sistem Informasi Kegiatan Mahasiswa</h1>
                                    </div>
                                    <form action="<?= base_url('/auth/login') ?>" method="POST">
                                        <div class="form-group position-relative has-icon-left mb-4">
                                            <input type="email" class="form-control form-control-xl" placeholder="Email" name="email" id="email">
                                            <div class="invalid-feedback">
                                            </div>
                                            <div class="form-control-icon">
                                                <i class="bi bi-envelope"></i>
                                            </div>
                                        </div>
                                        <div class="form-group position-relative has-icon-left mb-4">
                                            <input type="password" class="form-control form-control-xl" placeholder="Password" name="password" id="password">
                                            <div class="invalid-feedback">
                                            </div>
                                            <div class="form-control-icon">
                                                <i class="bi bi-shield-lock"></i>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5" type="submit">Log in</button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href=<?= base_url("/register") ?>>Belum memiliki Akun?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- Bootstrap core JavaScript-->
    <script src=<?= base_url("vendor/jquery/jquery.min.js") ?>></script>
    <script src=<?= base_url("vendor/bootstrap/js/bootstrap.bundle.min.js") ?>></script>

    <!-- Core plugin JavaScript-->
    <script src=<?= base_url("vendor/jquery-easing/jquery.easing.min.js") ?>></script>

    <!-- Custom scripts for all pages-->
    <script src=<?= base_url("js/sb-admin-2.min.js") ?>></script>


</body>

</html>