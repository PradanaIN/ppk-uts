<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container mt-5">

    <div class="row d-flex justify-content-center">

        <div class="col-md-7">

            <div class="card p-3 py-4">

                <div class="text-center">
                    <img src="https://riverlegacy.org/wp-content/uploads/2021/07/blank-profile-photo.jpeg" width="100" class="rounded-circle">
                </div>

                <div class="text-center mt-3">
                    <h5 class="mt-2 mb-0"><?= session()->get('username_user') ?></h5>
                    <span><?= session()->get('nama_user') ?></span>

                    <div class="buttons mt-3">

                        <a href=<?= base_url("/edit-profil") . "/" . session()->get('id_user') ?>>
                            <button class="btn btn-outline-primary px-4">Edit Profile</button>
                        </a>
                    </div>


                </div>




            </div>

        </div>

    </div>

</div>

<?= $this->endSection(); ?>